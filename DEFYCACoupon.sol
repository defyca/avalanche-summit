// SPDX-License-Identifier: none
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/draft-ERC20Permit.sol";
import "./ERC1404.sol";
import "./IMinterPermissionedTokensDEFYCA.sol";
import "./IStripsAndCouponsMinterDEFCYA.sol";


contract PermissionedTokenDEFYCACoupon is
    ERC20,
    ERC1404,
    ERC20Burnable,
    ERC20Permit
{
    uint256 private maturesOn; // Calculated based on the bond issuedOn and matures fields

    // The value of the strip in the denominated baseCurrency that must be transferred when the strip is minted, i.e bond "PURCHASED". It also represents the 0-Coupon value of the redemption of the strip on the specified maturesOn date.
    // An investor purchases a portion of a bond up to 100% of the value of the bond, this is known as the concentration ratio. When the DEFYCA auction occurs the Investor strips are minted according to this concentration ratio.
    uint256 private value;

    // The baseCurrency of the issuance, for now, we support USDC, DAI and USDT
    // Must be the same as the bond
    string private baseCurrency;
    address private adminContract = address(0x0);
    address private bondAddress;

    modifier onlyMinterDEFYCA() {
        require(IStripsAndCouponsMinterDEFCYA(adminContract).checkAdminPrivileges(msg.sender)==true);
        _;
    }

    constructor(
        uint256 _maturesOn,
        uint256 _value,
        uint256 _yield,
        string memory _baseCurrency,
        address _behalfOf,
        address _bondAddress
    )
        ERC20("PermissionedTokenDEFYCACoupon", "DFYCPN")
        ERC20Permit("PermissionedTokenDEFYCACoupon")
    {
        _mint(_behalfOf, _maturesOn);
        maturesOn = block.timestamp + (_maturesOn * 60 * 60 * 24); //_maturesOn *days* is added to current date
        value = (_value*(_yield*10**2))/_maturesOn; // the value in base currency
        baseCurrency = _baseCurrency; // the currency on which the coupon is based
        bondAddress = _bondAddress;
        adminContract = msg.sender;
    }
    // 1 token = 1 token no decimals
    function decimals() public view virtual override returns (uint8) {
        return 0;
    }

    function mint(address to, uint256 amount) public {
        _mint(to, amount);
    }

    // required override functions
    function detectTransferRestriction(
        address /*from*/,
        address /*to*/,
        uint256 /*valueTT*/
    ) public pure override returns (uint8) {
        return 0;
    }

    // required override functions
    function messageForTransferRestriction(uint8 /*restrictionCode*/)
        public
        pure
        override
        returns (string memory)
    {
        return "Coupon";
    }

    // Simple set AdminAddress
    function setAdminAddress(address AdminSet) external {
        require(adminContract == address(0x0), 'admin already set');
        adminContract = AdminSet;
    }

    // Setter Methods
    function setValue(uint256 _value) external onlyMinterDEFYCA {
        value = _value;
    }

    function setBaseCurrency(string memory _baseCurrency)
        external
        onlyMinterDEFYCA
    {
        baseCurrency = _baseCurrency;
    }

    function setMaturesOn(uint256 _maturesOn) external onlyMinterDEFYCA {
        maturesOn = _maturesOn;
    }

    //Getter Methods
    function getValue() external view returns (uint256) {
        return value;
    }

    function getBaseCurrency() external view returns (string memory) {
        return baseCurrency;
    }

    function getMaturesOn() external view returns (uint256) {
        return maturesOn;
    }

    function getBondAddress() external view returns (address) {
        return bondAddress;
    }


    // Override based on Hooks 
    function _beforeTokenTransfer(address from, address to, uint256 amount) internal virtual override {
        super._beforeTokenTransfer(from, to, amount);
        // check if to is on whitelist ...
        if (adminContract == address(0x0)) {
            require(IMinterPermissionedTokensDEFYCA(IStripsAndCouponsMinterDEFCYA(msg.sender).getBondMinter())
            .checkIfUserStripsAndCouponsCanMove(to), "cannot move funds");
        } else {
            require(IMinterPermissionedTokensDEFYCA(IStripsAndCouponsMinterDEFCYA(adminContract).getBondMinter())
            .checkIfUserStripsAndCouponsCanMove(to), "cannot move funds");
        }
    }

}