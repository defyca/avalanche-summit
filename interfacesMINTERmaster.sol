//SPDX-License-Identifier: none
pragma solidity 0.8.4;
// -- INTERFACES -- //
interface IMinterPermissionedTokensDEFYCA {
    function checkIfUserBondCanMove(address UserTocheck)
        external
        view
        returns (bool);
		
	function checkIfUserStripsAndCouponsCanMove(address UserTocheck)
		external
        view
        returns (bool);
		
	function checkAdminPrivelages(address SenderToCheck)
		external
        view
        returns (bool);
    function checkStateOfBond(uint256 BondIdtoCheck)
		external
        view
        returns (uint);


    function checkAddressOfBond(uint256 BondIdtoCheck)
		external
        view
        returns (address);

    function checkAddressOfCoupon(uint256 BondIdtoCheck)
		external
        view
        returns (address);

    function checkAddressOfStripe(uint256 BondIdtoCheck)
		external
        view
        returns (address);

    function setBondAddress(uint256 BondIndex,address BondAddress)
		external;

}

