//SPDX-License-Identifier: none
pragma solidity 0.8.4;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/draft-ERC20Permit.sol";
import "./interfacesMINTERmaster.sol";

abstract contract ERC1404 {
    /// @notice Detects if a transfer will be reverted and if so returns an appropriate reference code
    /// @param from Sending address
    /// @param to Receiving address
    /// @param value Amount of tokens being transferred
    /// @return Code by which to reference message for rejection reasoning
    /// @dev Overwrite with your custom transfer restriction logic
    function detectTransferRestriction(
        address from,
        address to,
        uint256 value
    ) public view virtual returns (uint8);

    /// @notice Returns a human-readable message for a given restriction code
    /// @param restrictionCode Identifier for looking up a message
    /// @return Text showing the restriction's reasoning
    /// @dev Overwrite with your custom message and restrictionCode handling
    function messageForTransferRestriction(uint8 restrictionCode)
        public
        view
        virtual
        returns (string memory);
}

// interface for Strip and Coupon
interface IstripCouponMinter {

    // Mint Strips and Coupons for the Bond on _bondIndex , _behalfOf (who gets the Strips / Coupons
    function mintStripsAndCoupons(
        uint256 _value,
        uint256 _bondIndex,
        address _behalfOf
    ) external returns (address CouponAdr, address StripAdr);
}
/**
    BOND CONTRACT
 */
contract PermissionedTokenDEFYCABond is
    ERC20,
    ERC1404,
    ERC20Burnable,
    ERC20Permit
{
    address public lastTransferTo;
    address public AdminContract;
    bytes32 public immutable baseCurrency ="0x55534443";//"USDC";
    // Model of the bonds
    struct BondInDEFYCA {
        // ( structure for Bond should be set when minted )
        // rezygnujemy z BondID na rzecz state?
        // mapping(uint256 => uint256) bondIDToState;
      //  uint256 BondId;
        uint256 createdOn; // now timestamp of block then transformed with https://github.com/bokkypoobah/BokkyPooBahsDateTimeLibrary to  = 2022-Jan-01;		// this is  transformed from Unix time to typical date in view timestamp is  since 01.01.1970
        uint256 maturity; //days will be changed to block number and unix block stamp with above library  = 365;;    // rewards distributed to that MonthNumber from initial Treasury assets
        string schedule; // example : CUPON
        string redemption; // MONTHLY or uint for number of days + 999 for MONTHLY 8888 for yearly etc ...
        string bondtype; // example type = FF-SP;
        uint256 value; // shoudl it will be also number of bonds minted ??
       // string baseCurrency; //= USDC;	// it might be also unmutable if all bonds are in usdc ?
        string form; // example  FIXED or FLOATING
        string BER; // example : BBB AAA
        uint256 yield; // calculated in % example = 5.20%; // display that way in view
        uint256 stateOfBond; //stateOFbond // 1 -> MINTED , 2 -> RATED , 3 -> PRICED , 4-> SUBSRIBED , 5 -> PURCHASED , 6 -> STRIPPED , 7 -> DISCARCHED , 8-> MATURED , 9 -> IMPAIRED , 10 -> REDEEMED
 
        uint256 issuedOn; // block timestamp will be converted to date with view
        uint256 poolIdCircle; // Investors pool Id correspond to the wallet address in circle and wallet id in circle
        // bool exists;                        //
        // bool moving;                         // if movable
    }

    // Bond fething
    mapping(uint256 => BondInDEFYCA) public getBondByIndex;
    uint256 public thisBondIndex;

   
    modifier onlyMinterDEFYCA() {
        require(IMinterPermissionedTokensDEFYCA(AdminContract).checkAdminPrivelages(msg.sender)==true);
        _;
    }

    // Check TokenBalance
    function getTokenBalance(address _token) public view returns(uint256) {
      return IERC20(_token).balanceOf(address(this));
    }

    constructor(
        uint256 BondIndex,
        uint256 maturityIN,
        string memory scheduleIN,
        string memory redemptionIN,
        string memory bondtypeIN,
        uint256 valueIN,
     //   string memory baseCurrencyIN,
        string memory formIN,
        string memory BERin,
        uint256 yieldIN,
        uint256 poolIdInCircleIN       
    )
        ERC20("PermissionedTokenDEFYCABond", "BondDE")
        ERC20Permit("PermissionedTokenDEFYCABond")
    {
        _mint(tx.origin, 1);

        thisBondIndex = BondIndex;
        getBondByIndex[BondIndex] = BondInDEFYCA(
          //  BondIndex,
            block.timestamp,
            maturityIN,
            scheduleIN,
            redemptionIN,
            bondtypeIN,
            valueIN,
          //  baseCurrencyIN,
            formIN,
            BERin,
            yieldIN,
            1,
            block.timestamp,
            poolIdInCircleIN
        );
    }

    // 1 token = 1 token no decimals
    function decimals() public view virtual override returns (uint8) {
        return 0;
    }

    function mint(address to, uint256 amount) public onlyMinterDEFYCA {
        _mint(to, amount);
    }

    // Security aspects implementaiton of 1404 ,whtielisting transfer restrictions and alert messages
    function detectTransferRestriction(
        address from,
        address to,
        uint256 value
    ) public pure override returns (uint8) {
        return 0;
    }

    function setAdminAddress(address AdminSet) external returns (bool) {
        if (AdminContract == address(0x0)) {
            AdminContract = AdminSet;
        }
        return true;
    }

    // Security aspects implementaiton of 1404 ,whtielisting transfer restrictions and alert messages
    function transfer(address to, uint256 amount)
        public
        virtual
        override
        returns (bool)
    {
        // check if to is on whitelist ...
        if (
            IMinterPermissionedTokensDEFYCA(AdminContract).checkIfUserBondCanMove(
                to
            )==false) {
            messageForTransferRestriction(0);
            revert();
        }

        address owner = _msgSender();
        _transfer(owner, to, amount);
        lastTransferTo = to;
        return true;
    }

    function transferFrom(
        address from,
        address to,
        uint256 amount
    ) public virtual override returns (bool) {
        if (
            IMinterPermissionedTokensDEFYCA(AdminContract).checkIfUserBondCanMove(
                to
            )==false) {
            messageForTransferRestriction(0);
            revert();
        }
        address spender = _msgSender();
        _spendAllowance(from, spender, amount);
        _transfer(from, to, amount);
        lastTransferTo = to;
        return true;
    }

    function messageForTransferRestriction(uint8 restrictionCode1)
        public
        view
        override
        returns (string memory)
    {
        return "Hello NoWay";
    }



    function getpoolIdCircle() external view returns (uint256) {
        return getBondByIndex[thisBondIndex].poolIdCircle;
    }
    
    function getMaturity() external view returns (uint256) {
        return getBondByIndex[thisBondIndex].maturity;
    }
function bytes32ToString(bytes32 _bytes32) public pure returns (string memory) {
        uint8 i = 0;
        while(i < 32 && _bytes32[i] != 0) {
            i++;
        }
        bytes memory bytesArray = new bytes(i);
        for (i = 0; i < 32 && _bytes32[i] != 0; i++) {
            bytesArray[i] = _bytes32[i];
        }
        return string(bytesArray);
    }

    function getBaseCurrency() external returns (string memory) {
        return bytes32ToString(baseCurrency);
    }





    function setBondId(uint256 BondIDin) public onlyMinterDEFYCA {
        //getBondByIndex[thisBondIndex].BondId = BondIDin;
        thisBondIndex=BondIDin;
        // getBondByIndex[thisBondIndex].state = 2;
    }
   function setIssueDate(uint256 TimeOfIssueInUnixFormat) public onlyMinterDEFYCA {
        getBondByIndex[thisBondIndex].issuedOn = TimeOfIssueInUnixFormat;
        // getBondByIndex[thisBondIndex].state = X?
    }
   function setCreatedOn(uint256 createdOnTimeInUnixFormat) public onlyMinterDEFYCA {
        getBondByIndex[thisBondIndex].createdOn = createdOnTimeInUnixFormat;
        // getBondByIndex[thisBondIndex].state = X?
    }

           function setMaturity(uint256 maturityTimeInUnixFormat) public onlyMinterDEFYCA {
        getBondByIndex[thisBondIndex].maturity = maturityTimeInUnixFormat;
        // getBondByIndex[thisBondIndex].state = X?
    }



    function setSchedule(string memory scheduleinput) public onlyMinterDEFYCA {
        getBondByIndex[thisBondIndex].schedule = scheduleinput;
        // getBondByIndex[thisBondIndex].state = X?
    }

    function setRedemption(string memory redemptioninput) public onlyMinterDEFYCA {
        getBondByIndex[thisBondIndex].redemption = redemptioninput;
        // getBondByIndex[thisBondIndex].state = X?
    }

         function setBondtype(string memory bondtypeinput) public onlyMinterDEFYCA {
        getBondByIndex[thisBondIndex].bondtype = bondtypeinput;
        // getBondByIndex[thisBondIndex].state = X?
    }
         function setValueInBond(uint256 valueIN) public onlyMinterDEFYCA {
        getBondByIndex[thisBondIndex].value = valueIN;
        // getBondByIndex[thisBondIndex].state = X?
    }

  
   //            function setBaseCurrency(string memory baseCurrencySET) public onlyMinterDEFYCA {
        //getBondByIndex[thisBondIndex].baseCurrency = baseCurrencySET;
        // getBondByIndex[thisBondIndex].state = X?
  //  }

  

     function setForm(string memory formSET) public onlyMinterDEFYCA {
        getBondByIndex[thisBondIndex].form = formSET;
        // getBondByIndex[thisBondIndex].state = X?
    }

    function setBER(string memory BERinput) public onlyMinterDEFYCA {
        getBondByIndex[thisBondIndex].BER = BERinput;
        // getBondByIndex[thisBondIndex].state = X?
    }
    function setYIELD(uint256 YieldInput) public onlyMinterDEFYCA {
        getBondByIndex[thisBondIndex].yield = YieldInput;
        // getBondByIndex[thisBondIndex].state = 3;
    }
    function getYield() external view returns(uint256) {
        return getBondByIndex[thisBondIndex].yield;
        // getBondByIndex[thisBondIndex].state = 3;
    }
}

contract minterPermissionedTokensBondsDEFYCA{


mapping(uint256 => uint) public  getStateOfBond;
  
    address public minterAdmin =address(0x0);


    uint256 public BondIndex = 0;
     address public   ServiceAdministrator =address(0x0);


    

    modifier onlyServiceADministrator() {
     IMinterPermissionedTokensDEFYCA(minterAdmin).checkAdminPrivelages(msg.sender);
     // Underscore is a special character only used inside
        // a function modifier and it tells Solidity to
        // execute the rest of the code.
        _;
    }
	// for exterlan calls whitelisted bots and administrators
	




    constructor(uint256 initialBondIndex,address MasterMinterRepo) {
        // in case of redeployment of minter
        BondIndex = initialBondIndex;
        ServiceAdministrator = msg.sender;
        minterAdmin=MasterMinterRepo;
    }
    

    function BondEmition(
        uint256 maturityIN,
        string memory scheduleIN,
        string memory redemptionIN,
        string memory bondtypeIN,
        uint256 valueIN,
      //  string memory baseCurrencyIN,
        string memory formIN,
        string memory BERin,
        uint256 yieldIN,
        uint256 poolIdInCircleIN
    )
        public
        returns (
            //address[] memory whitelistedUsersInit
            address BondAddressR
        )
    {
        PermissionedTokenDEFYCABond BondAddress = new PermissionedTokenDEFYCABond(
            BondIndex,
            maturityIN,
            scheduleIN,
            redemptionIN,
            bondtypeIN,
            valueIN,
        //    baseCurrencyIN,
            formIN,
            BERin,
            yieldIN,
          //  1,
            poolIdInCircleIN
            //whitelistedUsersInit
        );
        
BondAddressR=address(BondAddress);
   //?   
    IMinterPermissionedTokensDEFYCA(minterAdmin).setBondAddress(BondIndex,BondAddressR);
    BondIndex = BondIndex + 1;
    }

   
	

}
