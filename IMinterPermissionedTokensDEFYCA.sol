// SPDX-License-Identifier: None
pragma solidity ^0.8.4;


interface IMinterPermissionedTokensDEFYCA {
    function checkIfUserBondCanMove(address UserTocheck)
        external
        view
        returns (bool);
        
    function checkIfUserStripsAndCouponsCanMove(address UserTocheck)
        external
        view
        returns (bool);
        
    function checkAdminPrivilages(address SenderToCheck)
        external
        view
        returns (bool);
    function checkStateOfBond(uint256 BondIdtoCheck)
        external
        view
        returns (uint);
    function checkAddressOfBond(uint256 BondIdtoCheck)
        external
        view
        returns (address);
    function checkAddressOfCoupon(uint256 BondIdtoCheck)
        external
        view
        returns (address);
    function checkAddressOfStripe(uint256 BondIdtoCheck)
        external
        view
        returns (address);

  function checkIndexOfPoolInCircle(uint256 BondIdtoCheck)
		external
        view
        returns (uint256);

   function setPairBondPool(uint256 BondIdtoset,uint poolIdToSet)
		external
        returns(uint256,uint);

}

