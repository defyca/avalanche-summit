// SPDX-License-Identifier: none
pragma solidity^0.8.4;

import "./IMinterPermissionedTokensDEFYCA.sol";
import "./IBondInDEFYCA.sol";
import "./DEFYCAStrip.sol";
import "./DEFYCACoupon.sol";


contract stripsAndCouponsMinterDEFCYA{

    address public minterDEFYCA;
    mapping(address => address) public getStripToBondAddress;
    mapping(address => uint256) public getStripToBondIndex;
    mapping(address => address[]) public getBondAddressToStrip;
    mapping(address => address) public getCouponToBondAddress;
    mapping(address => uint256) public getCouponToBondIndex;
    mapping(address => address[]) public getBondAddressToCoupon;
    mapping(address => bool) public whitelistedBotsANDserviceAdmins;
    mapping(uint256 => address) public getStripAddressByIndex;
    mapping(address => uint256) public getStripIndexByAddress;
    mapping(uint256 => address) public getCouponAddressByIndex;
    mapping(address => uint256) public getCouponIndexByAddress;
    uint256 public stripIndex;
    uint256 public couponIndex;

    address public ServiceAdministrator;

    modifier adminPrivileges(address _addr) {
        require(whitelistedBotsANDserviceAdmins[_addr] == true, "Not valid sender for updates");
        _;
    }
    
    modifier onlyServiceADministrator() {
        require(msg.sender == ServiceAdministrator, "Not Admin");
        // Underscore is a special character only used inside
        // a function modifier and it tells Solidity to
        // execute the rest of the code.
        _;
    }

    constructor(address _minterDEFYCA) {
        minterDEFYCA = _minterDEFYCA;
        ServiceAdministrator = msg.sender;
    }

    function whitelistBotsANDserviceAdmins(address newBot)
        public
        onlyServiceADministrator
        returns (bool)
    {
        whitelistedBotsANDserviceAdmins[newBot] = true;
        return true;
    }
    // Check if bot or owner can update the Bonds and Coupons and Stripes values and more
    function checkAdminPrivileges(address senderToCheck)
        external
        view
        adminPrivileges(senderToCheck)
        returns (bool)
    {
        return true;
    }


    // Mint Strips and Coupons for the Bond on _bondIndex , _behalfOf (who gets the Strips / Coupons)
    function mintStripsAndCoupons(
        uint256 _value,
        uint256 _bondIndex,
        address _behalfOf
    ) public returns (address, address) {
        //Create new Strip based on the _bondIndex data
        PermissionedTokenDEFYCAStrip stripAddr = new PermissionedTokenDEFYCAStrip(
            IBondInDEFYCA(address(IMinterPermissionedTokensDEFYCA(minterDEFYCA).checkAddressOfBond(_bondIndex)))
                .getMaturity(),
            _value,
            IBondInDEFYCA(address(IMinterPermissionedTokensDEFYCA(minterDEFYCA).checkAddressOfBond(_bondIndex)))
                .getBaseCurrency(),
            _behalfOf,
            address(IMinterPermissionedTokensDEFYCA(minterDEFYCA).checkAddressOfBond(_bondIndex))
        );
        // Set the mappings for the Strips for BOND
        getStripToBondAddress[address(stripAddr)] = address(
            IMinterPermissionedTokensDEFYCA(minterDEFYCA).checkAddressOfBond(_bondIndex)
        );
        getStripToBondIndex[address(stripAddr)] = _bondIndex;
        getBondAddressToStrip[address(IMinterPermissionedTokensDEFYCA(minterDEFYCA).checkAddressOfBond(_bondIndex))].push(
            address(stripAddr)
        );

        getStripAddressByIndex[stripIndex] = address(stripAddr);
        getStripIndexByAddress[address(stripAddr)] = stripIndex;
        stripIndex = stripIndex+1;

        //Create new coupon based on the _bondIndex data
        PermissionedTokenDEFYCACoupon couponAddr = new PermissionedTokenDEFYCACoupon(
            IBondInDEFYCA(address(IMinterPermissionedTokensDEFYCA(minterDEFYCA).checkAddressOfBond(_bondIndex)))
                .getMaturity(),
            _value,
            IBondInDEFYCA(address(IMinterPermissionedTokensDEFYCA(minterDEFYCA).checkAddressOfBond(_bondIndex)))
                .getYield(),
            IBondInDEFYCA(address(IMinterPermissionedTokensDEFYCA(minterDEFYCA).checkAddressOfBond(_bondIndex)))
                .getBaseCurrency(),
            _behalfOf,
            address(IMinterPermissionedTokensDEFYCA(minterDEFYCA).checkAddressOfBond(_bondIndex))
        );
        // Set the mappings for the Coupons for BOND
        getCouponToBondAddress[address(couponAddr)] = address(
            IMinterPermissionedTokensDEFYCA(minterDEFYCA).checkAddressOfBond(_bondIndex)
        );
        getCouponToBondIndex[address(couponAddr)] = _bondIndex;
        getBondAddressToCoupon[address(IMinterPermissionedTokensDEFYCA(minterDEFYCA).checkAddressOfBond(_bondIndex))].push(
                address(couponAddr)
            );

        getCouponAddressByIndex[couponIndex] = address(couponAddr);
        getCouponIndexByAddress[address(couponAddr)] = couponIndex;
        couponIndex = couponIndex+1;

        return (address(stripAddr), address(couponAddr));
    }

    function getBondMinter() external view returns(address){
        return minterDEFYCA;
    }


    // SETTER METHODS
    function updateValueInStrip(
        uint256 _indexOfStripToBeUpdated,
        uint256 _newValue
    ) public returns (bool) {
        PermissionedTokenDEFYCAStrip(address(getStripAddressByIndex[_indexOfStripToBeUpdated])).setValue(_newValue);
        return true;
    }
    function updateValueInCoupon(
        uint256 _indexOfCouponToBeUpdated,
        uint256 _newValue
    ) public returns (bool) {
        PermissionedTokenDEFYCACoupon(address(getCouponAddressByIndex[_indexOfCouponToBeUpdated])).setValue(_newValue);
        return true;
    }

    function updateValueInBOTH(
        uint256 _indexOfBOTHToBeUpdated,
        uint256 _newValue
    ) public returns (bool) {
        PermissionedTokenDEFYCAStrip(address(getStripAddressByIndex[_indexOfBOTHToBeUpdated])).setValue(_newValue);
        PermissionedTokenDEFYCACoupon(address(getCouponAddressByIndex[_indexOfBOTHToBeUpdated])).setValue(_newValue);
        return true;
    }


    // SETTER METHODS
    function updateMaturesOnInStrip(
        uint256 _indexOfStripToBeUpdated,
        uint256 _newMaturesOn
    ) public returns (bool) {
        PermissionedTokenDEFYCAStrip(address(getStripAddressByIndex[_indexOfStripToBeUpdated])).setMaturesOn(_newMaturesOn);
        return true;
    }
    function updateMaturesOnInCoupon(
        uint256 _indexOfCouponToBeUpdated,
        uint256 _newMaturesOn
    ) public returns (bool) {
        PermissionedTokenDEFYCACoupon(address(getCouponAddressByIndex[_indexOfCouponToBeUpdated])).setMaturesOn(_newMaturesOn);
        return true;
    }

    function updateMaturesOnInBOTH(
        uint256 _indexOfBOTHToBeUpdated,
        uint256 _newMaturesOn
    ) public returns (bool) {
        PermissionedTokenDEFYCAStrip(address(getStripAddressByIndex[_indexOfBOTHToBeUpdated])).setMaturesOn(_newMaturesOn);
        PermissionedTokenDEFYCACoupon(address(getCouponAddressByIndex[_indexOfBOTHToBeUpdated])).setMaturesOn(_newMaturesOn);
        return true;
    }



}
