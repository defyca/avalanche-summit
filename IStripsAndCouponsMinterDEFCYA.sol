//SPDX-License-Identifier: none
pragma solidity ^0.8.4;

interface IStripsAndCouponsMinterDEFCYA{
    function getBondMinter() external view returns(address);
    function checkAdminPrivileges(address senderToCheck) external view returns (bool);
}