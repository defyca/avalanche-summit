# avalanche-summit

The worlds 1st stablecoin bond at AvalancheSummit

## What we have built at Ava Summit
DEFYCA (Definitive Yields in Crypto Assets) is a liquidity market which brings commercial paper onchain, providing Investors with a wrapped USDC yield, which is highly tradable and can always be settled with the stablecoin Issuer (Circle).

DEFYCA is a permissioned subnet using onChainIdentity validators, ensuring that both Investors and commercial paper Issuers are legally protected via full professional risk rating and securitization.

At the Avalanche Summit Hackathon, DEFYCA: 
Issued the world's first risk-priced stablecoin debt instrument. 
Deployed our smart contracts on an Avalanche subnet
Integrated our treasury via Circle
Demonstrated how Investors with Coinbase wallets can invest USDC and receive USDC wrapped yield bearing maturities.

DEFYCA has been proudly supported by NETI and Tokeny, specialists in development for institutional grade smart contracts.
