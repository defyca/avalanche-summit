//SPDX-License-Identifier: none
pragma solidity 0.8.4;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/draft-ERC20Permit.sol";
import "./interfacesMINTERmaster.sol";
import "./ComplyDeFi.sol";

abstract contract ERC1404 {
    /// @notice Detects if a transfer will be reverted and if so returns an appropriate reference code
    /// @param from Sending address
    /// @param to Receiving address
    /// @param value Amount of tokens being transferred
    /// @return Code by which to reference message for rejection reasoning
    /// @dev Overwrite with your custom transfer restriction logic
    function detectTransferRestriction(
        address from,
        address to,
        uint256 value
    ) public view virtual returns (uint8);

    /// @notice Returns a human-readable message for a given restriction code
    /// @param restrictionCode Identifier for looking up a message
    /// @return Text showing the restriction's reasoning
    /// @dev Overwrite with your custom message and restrictionCode handling
    function messageForTransferRestriction(uint8 restrictionCode)
        public
        view
        virtual
        returns (string memory);
}

// interface for Strip and Coupon
interface IstripCouponMinter {

    // Mint Strips and Coupons for the Bond on _bondIndex , _behalfOf (who gets the Strips / Coupons
    function mintStripsAndCoupons(
        uint256 _value,
        uint256 _bondIndex,
        address _behalfOf
    ) external returns (address CouponAdr, address StripAdr);
}

contract minterPermissionedTokensDEFYCA is ComplyDeFi {

mapping(uint256 => uint) public  getStateOfBond;
 address public stripeCouponsMinter =address(0x0);

	mapping(address => bool) public whitelistedBotsANDserviceAdmins;

    mapping(uint256 => uint256) public getPoolIdInCircleByBondID;
    // Bond fething
    mapping(uint256 => address) public getBondAddressByIndex;
    mapping(address => uint256) public getBondIndexByAddress;
    mapping(address => address) public getStripToBondAddress;
    mapping(uint256 => address ) public getStripByBondIndex;
    mapping(address => address[]) public getBondAddressToStrip;
    mapping(address => address) public getCouponToBondAddress;
    mapping(uint256 => address) public getCouponByBondIndex;
    mapping(address => address[]) public getBondAddressToCoupon;

    uint256 public BondIndex = 0;

    uint256 public usersIndex = 0;
    uint256 public usersIndexBonds = 0;
    address public ServiceAdministrator;
    uint256 public usersIndexStripesCoupons = 0;

    modifier onlyServiceADministrator() {
        require(msg.sender == ServiceAdministrator, "Not Admin");
        // Underscore is a special character only used inside
        // a function modifier and it tells Solidity to
        // execute the rest of the code.
        _;
    }
        modifier onlyWhitelistedAdminsOrigin() {
        require(whitelistedBotsANDserviceAdmins[tx.origin] == true, "Not Admin");
        // Underscore is a special character only used inside
        // a function modifier and it tells Solidity to
        // execute the rest of the code.
        _;
    }
	// for exterlan calls whitelisted bots and administrators
	
    modifier AdminPrivileges(address _addr) {
        require(whitelistedBotsANDserviceAdmins[_addr] == true, "Not valid sender for updates");
        _;
    }

    constructor(uint256 initialBondIndex, address idFactory) ComplyDeFi(idFactory){
        // in case of redeployment of minter
        BondIndex = initialBondIndex;
        ServiceAdministrator = msg.sender;
    }
    
    function setStripesCouponsMinter(address minterSC) public returns(bool){
if(stripeCouponsMinter==address(0x0)){
    stripeCouponsMinter=minterSC;
    return true;
} else {
    return false;
}


    }
    function checkAddressOfBond(uint256 BondIdtoCheck)
		external
        view
        returns (address){
    return getBondAddressByIndex[BondIdtoCheck];

        }

    function checkAddressOfCoupon(uint256 BondIdtoCheck)
		external
        view
        returns (address){
    return getCouponByBondIndex[BondIdtoCheck];

        }

    function checkAddressOfStripe(uint256 BondIdtoCheck)
		external
        view
        returns (address){

    return getStripByBondIndex[BondIdtoCheck];
        }

    function checkIndexOfPoolInCircle(uint256 BondIdtoCheck)
		external
        view
        returns (uint256){
 
    return getPoolIdInCircleByBondID[BondIdtoCheck];
        }

	 function checkStateOfBond(uint256 BondIdtoCheck)
		external
        view
        returns (uint){
        return getStateOfBond[BondIdtoCheck];
        }

        	 function setStateOfBond(uint256 BondIdtoset,uint stateOfBond)
		public
        returns(uint256,uint){
         getStateOfBond[BondIdtoset]=stateOfBond;
         return (BondIdtoset,stateOfBond);
        }

        	 function setPairBondPool(uint256 BondIdtoset,uint poolIdToSet)
		public
        returns(uint256,uint){
         getPoolIdInCircleByBondID[BondIdtoset]=poolIdToSet;
         return (BondIdtoset,poolIdToSet);
        }
 
    
    function whitelistBotsANDserviceAdmins(address newBot)
        public
        onlyServiceADministrator
        returns (bool)
    {
        whitelistedBotsANDserviceAdmins[newBot] = true;
        return true;
    }


    function setBondAddress(uint256 aBondIndex,address aBondAddress) onlyWhitelistedAdminsOrigin external {
        
        getBondAddressByIndex[aBondIndex] = address(aBondAddress);
        getBondIndexByAddress[address(aBondAddress)] = aBondIndex;

      
    }

    // Check if Bonds can be moved
    function checkIfUserBondCanMove(address userToCheck)
        external
        view
        returns (bool)
    {
        if(isComply(userToCheck)){
            return true;
        }
        return false;
    }

    // Check if Strips or Coupons can be moved
    function checkIfUserStripsAndCouponsCanMove(address userToCheck)
        external
        view
        returns (bool)
    {
        if(isComply(userToCheck)){
            return true;
        }
        return false;
    }
	
	    // Check if bot or owner can update the Bonds and Coupons and Stripes values and more
    function checkAdminPrivileges(address senderToCheck)
        external
        view
        AdminPrivileges(senderToCheck)
        returns (bool)
    {
        return true;
    }


}
