// SPDX-License-Identifier: None
pragma solidity ^0.8.4;


interface IBondInDEFYCA {
    function decimals() external view  returns (uint8);
    function detectTransferRestriction(address from,address to,uint256 value) external pure returns (uint8);
    function getMaturity() external view returns (uint256);
    function getBaseCurrency() external view returns (string memory);
    function getpoolIdCircle() external returns (uint256);
    function getYield() external view returns(uint256);

}
